var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasapiRouter = require('./routes/api/bicicletas');
var usuariosapiRouter = require('./routes/api/usuarios');

var app = express();

var mongoose = require('mongoose');

/** Me conecto a Atlas porque no me permite instalar mongo en mi maquina porque tengo windows 7
 *  Muchas Gracias
 */

var mongoDBPath = "mongodb+srv://christian:christian@redbicicletas.qkxqb.mongodb.net/Bicicleta?retryWrites=true&w=majority";
mongoose.connect(mongoDBPath, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.on('connected', () => {
  console.log('Conexion a Base de Datos Correcta');
})

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas', bicicletasapiRouter);
app.use('/api/usuarios', usuariosapiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
