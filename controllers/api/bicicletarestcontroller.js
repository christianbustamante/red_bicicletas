var Bicicleta = require('../../models/bicicletamodel');

exports.BicicletaList = function (req,res) {
    Bicicleta.getAll(function (error, result) {
        res.status(200).json({
            data: result
        });  
    });
}

exports.BicicletaCreate = function (req, res) { 
    let bicicleta = new Bicicleta({
        codigo: req.body.id, 
        color: req.body.color, 
        modelo: req.body.model,
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicleta.add(bicicleta, function (error, newElement) { 
        res.status(200).json({
            data: newElement
        });
    })
}

exports.BicicletaDelete = function (req, res) { 
    Bicicleta.removeByCodigo(req.body.code, function (err) {
        res.status(204).send();
    });
}