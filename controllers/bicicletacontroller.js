var Bicicleta = require('../models/bicicletamodel');

exports.bicicletaList =  function (req,res) {
    Bicicleta.getAll(function (error, result) {
        res.render('bicicletas/index', {data: result});
    });
}

exports.bicicletaCreateGet = function (req, res) { 
    res.render('bicicletas/create')
}

exports.bicicletaCreatePost = function (req, res) {
    let bicicleta = new Bicicleta({
        codigo: req.body.id, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicleta.add(bicicleta, function (error, newElement) { 
        res.redirect('/bicicletas');
    });
}

exports.bicicletaUpdateGet = function (req, res) {
    Bicicleta.findByCodigo(req.params.codigo, function(err,bicicleta){
        res.render('bicicletas/update', {data : bicicleta});
    }); 
}

exports.bicicletaUpdatePost = function (req, res) { 
    Bicicleta.findByCodigo(req.params.codigo, function (err, bicicleta) {
        bicicleta.color = req.body.color;
        bicicleta.modelo = req.body.modelo;
        bicicleta.ubicacion = [req.body.lat, req.body.lng];
        bicicleta.save();
        
        res.redirect('/bicicletas');
    });
    
}

exports.bicicletaDeletePost = function (req, res) { 
    Bicicleta.removeByCodigo(req.body.codigo, function (err) {
        res.redirect('/bicicletas');
    });
}