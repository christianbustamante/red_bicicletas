let mongoose = require('mongoose');
let schema = mongoose.Schema;

let bicicletaSchema = new schema({
    codigo: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function (codigo, color, modelo, ubicacion) {  
    return new this({
        codigo: codigo,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    })
};

bicicletaSchema.statics.updateInstance = function (codigo, newcodigo, color, modelo, ubicacion) {  
    this.findOne({ codigo: codigo }, function (err, doc){
        doc.codigo = newcodigo;
        doc.color = color;
        doc.model = modelo;
        doc.ubicacion = ubicacion;
        doc.save();
    });
};

bicicletaSchema.methods.toString = function () {  
    return 'codigo: ' + this.codigo + " | color: " + this.color;
};

bicicletaSchema.statics.getAll = function (cb) {  
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function (bicicleta, cb) {  
    this.create(bicicleta, cb);
}

bicicletaSchema.statics.findByCodigo = function (codigo, cb) {  
    return this.findOne({codigo: codigo}, cb);
}

bicicletaSchema.statics.removeByCodigo = function (codigo, cb) {  
    return this.deleteOne({codigo: codigo}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);