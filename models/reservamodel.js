let mongoose = require('mongoose');
let schema = mongoose.Schema;
let moment = require('moment');

let Reserva = new schema({
    desde: Date,
    hasta: Date,
    bicicleta: {type: schema.Types.ObjectId, ref: 'Bicicleta'},
    usuario: {type: schema.Types.ObjectId, ref: 'Usuario'}
});

Reserva.methods.reservedDays = function () {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

module.exports = mongoose.model('Reserva', Reserva);