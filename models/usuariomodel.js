let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Reservamodel = require('./reservamodel');

var usuarioSchema = new Schema({
    nombre: String
});

usuarioSchema.methods.reserva = function (bicicletaId, desde, hasta, cb) {  
    let reserva = new Reservamodel({
        usuario: this._id,
        bicicleta: bicicletaId, 
        desde: desde,
        hasta: hasta
    });

    reserva.save(cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema);