var express = require('express');
var router = express.Router();

var bicicletaController = require('../../controllers/api/bicicletarestcontroller');

router.get('/', bicicletaController.BicicletaList);
router.post('/create', bicicletaController.BicicletaCreate);
router.delete('/delete', bicicletaController.BicicletaDelete);

module.exports = router;