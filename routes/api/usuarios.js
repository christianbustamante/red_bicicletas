let express = require('express');
let router = express.Router();
let usuarioController = require('../../controllers/api/usuariorestcontroller');

router.get('/', usuarioController.usuarioList);
router.post('/create', usuarioController.usuarioCreate);
router.post('/reserva', usuarioController.usuarioReserva);

module.exports = router;