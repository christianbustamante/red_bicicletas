var express = require('express');
var bicicletaController = require('../controllers/bicicletacontroller');

var router = express.Router();

router.get('/', bicicletaController.bicicletaList);
router.get('/create', bicicletaController.bicicletaCreateGet);
router.post('/create', bicicletaController.bicicletaCreatePost);
router.post('/:codigo/delete', bicicletaController.bicicletaDeletePost);
router.get('/:codigo/update', bicicletaController.bicicletaUpdateGet);
router.post('/:codigo/update', bicicletaController.bicicletaUpdatePost);

module.exports = router;