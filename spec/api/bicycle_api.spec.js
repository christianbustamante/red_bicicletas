let Bicycle = require('../../models/bicicletamodel');
let mongoose = require('mongoose');
let request = require('request');
let server  = require('../../bin/www');
const urlServer = 'http://localhost:3000/api/bicicletas'


describe('test API Bicicletas', () => {
    beforeEach(function (done) {
        mongoose.connection.close().then(() => {
            let mongoDB = "mongodb+srv://christian:christian@redbicicletas.qkxqb.mongodb.net/Bicicleta?retryWrites=true&w=majority";
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
    
            let db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });

        });

    });

    afterEach(function(done) {
        Bicycle.deleteMany({}, function (error, success) {  
            if(error) console.log('error');
            console.log(success)
            done();
        });   
    });

    describe("POST /create", ()=> {
        it("Status 200", (done)=> {
            let headers = {'content-type': 'application/json'};
            let bicycle = '{"id":3,"color":"rojo","modelo":"Urbana","lat":27.852403,"lng":-15.439928}';
            request.post({
                headers: headers,
                url: urlServer + '/create',
                body: bicycle
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                console.log(body)
                let b = JSON.parse(body);
                expect(b.data.color).toBe("rojo");
                /* expect(b.data.ubicacion[0]).toBe("27.852403");
                expect(b.data.ubicacion[1]).toBe("-15.439928"); */
                done();
            });
        });
    });

    describe('GET', () => {
        it('Status 200', (done) => {
            request.get(urlServer, function (error, res, body) { 
                console.log(body)
                console.log(res.statusCode) 
                expect(res.statusCode).toBe(200);
                let result = JSON.parse(body);
                expect(result.data.length).toBe(0);
                done();
            });
        });
    });
});

