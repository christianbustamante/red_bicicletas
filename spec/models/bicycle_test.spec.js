let mongoose = require('mongoose');
let Bicycle = require('../../models/bicicletamodel');

describe('Testing DB', () => {

    beforeAll((done) => { mongoose.connection.close(done) });


    beforeEach(function (done) {
        mongoose.disconnect();

        let mongoDBPath = `mongodb+srv://christian:christian@redbicicletas.qkxqb.mongodb.net/Bicicleta?retryWrites=true&w=majority`;
        mongoose.connect(mongoDBPath, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        mongoose.connection.on('connected', () => {
            console.log('MongoDB is connected');
            done();
        });

    });

    afterEach(function(done) {
        Bicycle.deleteMany({}, function (error, success) {  
            if(error) console.log('error');
            done();
        });   
    });

    describe('Create Test', () => {
        it('create a instance of one elemente', () => {
            let bicycle = Bicycle.createInstance(1, "verde" , "urbana",[1,-1]);

            expect(bicycle.codigo).toBe(1);
            expect(bicycle.color).toBe("verde");
            expect(bicycle.modelo).toBe("urbana");
            expect(bicycle.ubicacion[0]).toBe(1);
            expect(bicycle.ubicacion[1]).toBe(-1);
        });
    });

    describe('Get list of elemente', () => {
        it('start empty',(done) =>{
            Bicycle.getAll(function(err,result){
                expect(result.length).toBe(0);
                done();
            });            
        });
    });

    describe('Add element',() => {
        it('Add only one element', (done) => {
            let element = new Bicycle({codigo: 1, color: "verde", modelo: "urbano"});
            Bicycle.add(element, function(err, success){
                if (err) console.log(err);
                Bicycle.getAll(function(err,result){
                    expect(result.length).toBe(1);
                    expect(result[0].codigo).toBe(1);
                    done();
                }); 
            });            
        });        
    });

    describe('Find element',() => {
        it('Find By Code', (done) => {
            Bicycle.getAll(function (error, result) {  
                expect(result.length).toBe(0);
                let element1 = new Bicycle({codigo: 1, color: "verde", modelo: "urbano"});
                let element2 = new Bicycle({codigo: 2, color: "azul", modelo: "urbano"});
                Bicycle.add(element1, function(err, success){
                    if (err) console.log(err);
                    Bicycle.add(element2, function(err, success){
                        if (err) console.log(err);
                        Bicycle.findByCodigo(1,function(err,result){
                            expect(result.codigo).toBe(1);
                            expect(result.color).toBe(element1.color);
                            done();
                        }); 
                    });
                });   

            })   
        });        
    });

    describe('Remove element',() => {
        it('Remove By Code', (done) => {
            Bicycle.getAll(function (error, result) {  
                expect(result.length).toBe(0);
                let element1 = new Bicycle({codigo: 1, color: "verde", modelo: "urbano"});
                let element2 = new Bicycle({codigo: 2, color: "azul", modelo: "urbano"});
                Bicycle.add(element1, function(err, success){
                    if (err) console.log(err);
                    Bicycle.add(element2, function(err, success){
                        if (err) console.log(err);
                        Bicycle.getAll(function(err,result){
                            expect(result.length).toBe(2);
                            Bicycle.removeByCodigo(1,function(err,result){
                                Bicycle.getAll(function(err,result){
                                    expect(result.length).toBe(1);
                                    expect(result[0].color).toBe(element2.color);
                                    expect(result[0].codigo).toBe(element2.codigo);

                                    done();
                                }); 
                                
                            }); 
                        }); 
                    });
                });   

            })   
        });        
    });
});
