let mongoose = require('mongoose');
let User = require('../../models/usuariomodel');
let Bicycle = require('../../models/bicicletamodel');
let Booking = require('../../models/reservamodel');


describe('Testing Usuarios', () => {
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach( (done) => {
        mongoose.disconnect();

        let mongoDBPath = "mongodb+srv://christian:christian@redbicicletas.qkxqb.mongodb.net/Bicicleta?retryWrites=true&w=majority";
        mongoose.connect(mongoDBPath, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        mongoose.connection.on('connected', () => {
            console.log('MongoDB is connected');
            done();
        });
    });

    afterEach( (done) => {
        Booking.deleteMany({}, function (err, success) {
            if (err) {
                console.log(err);
            }

            User.deleteMany({}, function (err, success) {
                if (err) {
                    console.log(err);
                }

                Bicycle.deleteMany({}, function (err, success) {
                    if (err) {
                        console.log(err);
                    }
                    
                    mongoose.connection.close(done);
                });
            })
        });
    });

    describe('Cuando un Usuario reserva una Bici', () => {
        it('Debe existir la reserva', (done) => {
            const user = new User({ nombre: 'Julian' });
            user.save();

            const bicycle = new Bicycle({ codigo: 1, color: 'verde', modelo: 'urbana' });
            bicycle.save();

            let today = new Date();
            let tomorrow = new Date();
            tomorrow.setDate(today.getDate() + 1);

            user.reserva(bicycle.id, today, tomorrow, function (err, booking) {
                Booking.find({}).populate('bicicleta').populate('usuario').exec(function (err, bookings) {
                    console.log(bookings)
                    expect(bookings.length).toBe(1);
                    expect(bookings[0].reservedDays()).toBe(2);
                    expect(bookings[0].bicicleta.codigo).toBe(1);
                    expect(bookings[0].usuario.nombre).toBe(user.nombre);

                    done();
                });
            });
        });
    });


});